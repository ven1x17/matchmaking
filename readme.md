Java8 Akka Matchmaking project
----------------------------------
Tasks:

.skeleton

+ create modules for frontend and backend
+ create simple skeleton for backend with spring and akka (+remote)
+ create simple skeleton for frontend (rest)
+ add homepage web control panel

.adding technology

+ connect akka remote
+ h2-server starting with a shell script
- spring data configuration
- start akka system with the frontend and implement simple request from homepage to backend

.logic

- define unified REST interface to abstractly work with the system
- define backend interfaces for actor model based matchmaking system
- define and implement entities
- implement REST interface

.add features

- implement 1v1 matchmaking
- implement XvX matchmaking
- implement group(party) matchmaking
- implement multi-addon mode (for example custom maps)

.low priority features

- add possibility to start backend app in the different modes

----------------------------------
. - task block
+ - done

----------------------------------
Dev launch instructions:

1. Run backend/db/h2-server script
2. Deploy frontend war on a local container
3. Start backend entry point