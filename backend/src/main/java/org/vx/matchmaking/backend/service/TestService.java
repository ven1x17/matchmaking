package org.vx.matchmaking.backend.service;

import org.springframework.stereotype.Service;

@Service
public class TestService {

	public String testMethod() {
		return String.valueOf(Math.random());
	}

}
