package org.vx.matchmaking.backend;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.vx.matchmaking.backend.akka.message.TestMessage;
import org.vx.matchmaking.backend.configuration.SpringExtension;
import org.vx.matchmaking.backend.persistance.entity.Ent;
import org.vx.matchmaking.backend.persistance.service.EntService;

public class App 
{
    public static void main( String[] args )
    {
        AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext();
        ctx.scan("org.vx.matchmaking.backend");
        ctx.refresh();

        EntService service = ctx.getBean(EntService.class);
        service.deleteAll();
        service.addEnt(new Ent("ent A", "some message"));
        System.out.println("!!!" + service.getAllEnts());

        final ActorSystem system = ctx.getBean(ActorSystem.class);
        ActorRef dispatcher = system.actorOf(SpringExtension.SpringExtProvider.get(system).props("dispatcherActor"), "dispatcher");

        dispatcher.tell(new TestMessage(), null);


    }
}
