package org.vx.matchmaking.backend.akka.message.remote;

import java.io.Serializable;

public class LocalToRemoteMessage implements Serializable {
	private static final long serialVersionUID = 1;

	public final Object message;

	public LocalToRemoteMessage(Object message) {
		this.message = message;
	}
}
