package org.vx.matchmaking.backend.persistance.dao;

import org.springframework.data.repository.CrudRepository;
import org.vx.matchmaking.backend.persistance.entity.Ent;

public interface EntDao extends CrudRepository<Ent, Long> {
}
