package org.vx.matchmaking.backend.persistance.service;

import java.util.List;

import org.vx.matchmaking.backend.persistance.entity.Ent;

public interface EntService {

	public void addEnt(Ent ent);

	public Ent getEntById(int id);

	public List<Ent> getAllEnts();

	public void deleteAll();

}
