package org.vx.matchmaking.backend.akka.actor;

import akka.actor.UntypedActor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.vx.matchmaking.backend.akka.message.TestMessage;
import org.vx.matchmaking.backend.akka.message.remote.RemoteToLocalMessage;
import org.vx.matchmaking.backend.service.TestService;

@Component("dispatcherActor")
@Scope("prototype")
public class DispatcherActor extends UntypedActor {

	@Autowired
	private TestService testService;

	@Override
	public void onReceive(Object message) throws Exception {
		if (message instanceof TestMessage) {
			System.out.println(testService.testMethod());
			sender().tell(new RemoteToLocalMessage(new TestMessage()), self());
		} else {
			unhandled(message);
		}
	}

	@Override
	public void unhandled(Object message) {
		System.out.println("Unhandled message " + message);
	}
}
