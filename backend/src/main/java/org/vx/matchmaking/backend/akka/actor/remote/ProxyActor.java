package org.vx.matchmaking.backend.akka.actor.remote;

import akka.actor.ActorIdentity;
import akka.actor.ActorRef;
import akka.actor.Identify;
import akka.actor.ReceiveTimeout;
import akka.actor.Terminated;
import akka.actor.UntypedActor;
import akka.japi.Procedure;
import scala.concurrent.duration.Duration;

import org.vx.matchmaking.backend.akka.message.remote.LocalToRemoteMessage;
import org.vx.matchmaking.backend.akka.message.remote.RemoteToLocalMessage;

import static java.util.concurrent.TimeUnit.SECONDS;

/**
 * Proxy actor that forwards messages between local actor and remote one
 */
public class ProxyActor extends UntypedActor {

	private final String path;
	private ActorRef remoteActor = null;
	private ActorRef localActor;

	/**
	 * @param path path of the remote actor (for ex : "akka.tcp://HelloAkka@127.0.0.1:2552/user/greeter")
	 * @param localActor local actor to forward all remote messages to
	 */
	public ProxyActor(String path, ActorRef localActor) {
		this.localActor = localActor;
		this.path = path;
		sendIdentifyRequest();
	}

	private void sendIdentifyRequest() {
		getContext().actorSelection(path).tell(new Identify(path), getSelf());
		getContext()
				.system()
				.scheduler()
				.scheduleOnce(Duration.create(3, SECONDS), getSelf(),
						ReceiveTimeout.getInstance(), getContext().dispatcher(), getSelf());
	}

	@Override
	public void onReceive(Object message) throws Exception {
		if (message instanceof ActorIdentity) {
			remoteActor = ((ActorIdentity) message).getRef();
			if (remoteActor == null) {
				System.out.println("Remote actor not available: " + path);
			} else {
				getContext().watch(remoteActor);
				// switching onReceive implementation
				getContext().become(active, true);
				System.out.println("Connected and watching remote actor");
			}

		} else if (message instanceof ReceiveTimeout) {
			sendIdentifyRequest();

		} else {
			System.out.println("Not ready yet");

		}
	}

	Procedure<Object> active = message -> {
		if (message instanceof RemoteToLocalMessage) {
			System.out.println("Proxy actor > forwarding message to the local actor " + localActor);
			localActor.tell(((RemoteToLocalMessage) message).message, getSelf());
		} else if (message instanceof Terminated) {
			System.out.println("Proxy actor > Remote actor terminated");
			sendIdentifyRequest();
			getContext().unbecome();

		} else if (message instanceof LocalToRemoteMessage) {
			System.out.println("Proxy actor > forwarding message to the remote actor " + remoteActor);
			remoteActor.tell(((LocalToRemoteMessage) message).message, getSelf());
		} else if (message instanceof ReceiveTimeout) {
			// ignore

		} else {
			System.out.println("Unhandled message " + message);
		}

	};
}
