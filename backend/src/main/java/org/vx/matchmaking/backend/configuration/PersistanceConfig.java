package org.vx.matchmaking.backend.configuration;

import java.beans.PropertyVetoException;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.mchange.v2.c3p0.ComboPooledDataSource;

@Configuration
@EnableJpaRepositories("org.vx.matchmaking.backend.persistance.dao")
@EnableTransactionManagement
@PropertySource("classpath:hibernate.properties")
public class PersistanceConfig {

	// Jpa + hibernate
	@Bean
	public ComboPooledDataSource dataSource() throws PropertyVetoException {
		ComboPooledDataSource dataSource = new ComboPooledDataSource();
		dataSource.setUser("sa");
		dataSource.setPassword("");
		dataSource.setDriverClass("org.h2.Driver");
		dataSource.setJdbcUrl("jdbc:h2:tcp://localhost:8043/./test");
		dataSource.setInitialPoolSize(1);
		dataSource.setMaxPoolSize(2);
		dataSource.setMinPoolSize(1);
		dataSource.setAcquireIncrement(1);
		dataSource.setTestConnectionOnCheckin(false);
		dataSource.setTestConnectionOnCheckout(true);
		dataSource.setMaxIdleTime(1800);
		dataSource.setIdleConnectionTestPeriod(1000);
		return dataSource;
	}

	@Bean
	public LocalContainerEntityManagerFactoryBean entityManagerFactory() throws PropertyVetoException {
		LocalContainerEntityManagerFactoryBean entityManagerFactoryBean = new LocalContainerEntityManagerFactoryBean();

		HibernateJpaVendorAdapter jpaVendorAdapter = new HibernateJpaVendorAdapter();
		jpaVendorAdapter.setDatabasePlatform("org.hibernate.dialect.H2Dialect");
		jpaVendorAdapter.setShowSql(true);
		jpaVendorAdapter.setGenerateDdl(true);

		entityManagerFactoryBean.setPersistenceXmlLocation("classpath*:META-INF/persistence.xml");
		entityManagerFactoryBean.setPersistenceUnitName("mmpersistence");
		entityManagerFactoryBean.setDataSource(dataSource());
		entityManagerFactoryBean.setJpaVendorAdapter(jpaVendorAdapter);

		return entityManagerFactoryBean;
	}

	@Bean
	public JpaTransactionManager transactionManager() throws PropertyVetoException {
		JpaTransactionManager transactionManager = new JpaTransactionManager();
		transactionManager.setEntityManagerFactory(entityManagerFactory().getObject());
		return transactionManager;
	}

	@Bean
	public PersistenceExceptionTranslationPostProcessor persistenceExceptionTranslationPostProcessor(){
		return new PersistenceExceptionTranslationPostProcessor();
	}

}
