package org.vx.matchmaking.backend.akka.message.remote;

import java.io.Serializable;

public class RemoteToLocalMessage implements Serializable {
	private static final long serialVersionUID = 1;

	public final Object message;

	public RemoteToLocalMessage(Object message) {
		this.message = message;
	}
}
