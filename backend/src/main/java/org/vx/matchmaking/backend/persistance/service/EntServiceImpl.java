package org.vx.matchmaking.backend.persistance.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.vx.matchmaking.backend.persistance.dao.EntDao;
import org.vx.matchmaking.backend.persistance.entity.Ent;

@Transactional
@Service
public class EntServiceImpl implements EntService {

    @Autowired
    EntDao entDao;

    @Override
    public void addEnt(Ent ent) {
        entDao.save(ent);
    }

    @Override
    public Ent getEntById(int id) {
        return entDao.findOne((long)id);
    }

    @Override
    public List<Ent> getAllEnts() {
        List<Ent> result = new ArrayList<Ent>();
        for(Ent e: entDao.findAll()){
            result.add(e);
        }
        return result;
    }

    @Override
    public void deleteAll() {
        entDao.deleteAll();
    }
}
