package org.vx.matchmaking.frontend.rest;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.core.Application;

public class RestApplication extends Application {

	private static final Set<Object> SINGLETONS = new HashSet();
	private final Set<Class<?>> empty = new HashSet();


	public RestApplication() {
		// ADD RESTFUL RESOURCES HERE
		this.SINGLETONS.add(new RestResource());
	}

	public Set<Object> getSingletons() {
		return new HashSet<Object>(this.SINGLETONS);
	}

	public Set<Class<?>> getClasses() {
		return this.empty;
	}
}
