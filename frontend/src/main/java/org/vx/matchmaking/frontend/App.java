package org.vx.matchmaking.frontend;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import scala.concurrent.duration.Duration;

import java.util.concurrent.TimeUnit;

import org.vx.matchmaking.backend.akka.actor.remote.ProxyActor;
import org.vx.matchmaking.backend.akka.message.TestMessage;
import org.vx.matchmaking.backend.akka.message.remote.LocalToRemoteMessage;
import org.vx.matchmaking.frontend.akka.actor.ClientDispatcherActor;

import com.typesafe.config.ConfigFactory;

public class App {
	public static void main(String[] args) {
		final ActorSystem system = ActorSystem.create("RemoteSystem", ConfigFactory.load("remote"));

		final String path = "akka.tcp://MatchmakingBackend@127.0.0.1:2552/user/dispatcher";
		final ActorRef clientDispatcher = system.actorOf(Props.create(ClientDispatcherActor.class), "clientDispatcher");
		final ActorRef proxyActor = system.actorOf(
				Props.create(ProxyActor.class, path, clientDispatcher), "proxyActor");

		system.scheduler().schedule(Duration.Zero(), Duration.create(2, TimeUnit.SECONDS), proxyActor, new LocalToRemoteMessage(new TestMessage()),
				system.dispatcher(), clientDispatcher);
	}
}
