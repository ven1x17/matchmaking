package org.vx.matchmaking.frontend.rest;

import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

/// The Java class will be hosted at the URI path "/helloworld"
@Path("/helloworld")
public class RestResource {
	// The Java method will process HTTP GET requests
	@GET
	// The Java method will produce content identified by the MIME Media type "text/plain"
	@Produces("text/plain")
	public String getClichedMessage() {
		// Return some cliched textual content
		return "Hello World";
	}

	@GET
	@Path("/{parameter}")
	public Response responseMsg(@PathParam("parameter") String parameter,
			@DefaultValue("Nothing to say") @QueryParam("value") String value) {

		String output = "Hello from: " + parameter + " : " + value;
		return Response.status(200).entity(output).build();
	}

}
