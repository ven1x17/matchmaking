define(['./../config/controllers/module'], function (controllers) {
    'use strict';
    controllers.controller('mmController', ['$scope', '$rootScope', '$log', '$interval', '$http', 'noty', '$sce', '$compile',
        function($scope, $rootScope, $log, $interval, $http, noty, $sce, $compile) {

            $scope.noty = noty; // notify service
            $scope.testText = '';

            var REST_URL = 'rest/';

            // ================================================================

            $scope.testFunc = function() {
                $http.get(REST_URL + 'helloworld/123').success(function(data) {
                    $scope.testText = data;
                }).error(function(data, status, headers, config) {
                    $scope.showError(data);
                });
            };

            $scope.showError = function (string) {
                noty.error('Failed', string.substr(0, 200));
            };

            // ================================================================
            // on startup
            noty.info('loaded', "RequireJS has managed to load this mess");
        }]);
});