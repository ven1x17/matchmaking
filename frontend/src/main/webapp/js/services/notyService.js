define(['./../config/services/module'], function (services) {
    'use strict';
    services.service('noty', ['$rootScope', '$sce',
        function( $rootScope, $sce ) {

            var queue = [];

            return {
                queue: queue,

                add: function (item) {
                    item.timestamp = new Date().getTime();
                    queue.push(item);

                    var scope = this;
                    setTimeout(function () {
                        // remove the alert after 10s
                        var msg = $('#msg-' + item.timestamp);
                        if (msg != null) {
                            msg.remove();
                        }
                        scope.remove(item.timestamp);
                    }, 10000);
                },
                pop: function () {
                    return this.queue.pop();
                },
                remove: function(timestamp) {
                    for (var i = 0; i < queue.length; i++) {
                        var item = queue[i];
                        if (item.timestamp == timestamp) {
                            var msg = $('#msg-' + item.timestamp);
                            if (msg != null) {
                                msg.remove();
                            }
                            queue.splice(i, 1);
                            break;
                        }
                    }
                },
                success: function(title, message) {
                    this.add({
                        type: 'success',
                        title: title,
                        body: $sce.trustAsHtml(message)
                    })
                },
                info: function(title, message) {
                    this.add({
                        type: 'info',
                        title: title,
                        body: $sce.trustAsHtml(message)
                    })
                },
                warn: function(title, message) {
                    this.add({
                        type: 'warning',
                        title: title,
                        body: $sce.trustAsHtml(message)
                    })
                },
                error: function(title, message) {
                    this.add({
                        type: 'danger',
                        title: title,
                        body: $sce.trustAsHtml(message)
                    })
                }
            };
        }]);
});