define([
    'angular',
    './controllers/controllers',
    './services/services'
], function (ng) {
    'use strict';

    return ng.module('app', [
        'app.controllers',
        'app.services'
    ]);
});