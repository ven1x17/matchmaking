define(['./module'], function (controllers) {
    'use strict';
    controllers.controller('indexController', ['$scope', '$location', 'noty',
        function ($scope, $location, noty) {
            $scope.nav = function (path) {
                $location.path(path);
            };

            $scope.noty = noty; // notify service

        }]);
});