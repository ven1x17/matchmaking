require.config({

    paths: {
        'jQuery': '../../frameworks/jquery/jquery',
        'angular': '../../frameworks/angular/angular',
        'angular-route': '../../frameworks/angular-route/angular-route',
        'domReady': '../../frameworks/requirejs-domready/domReady',
        'jBootstrap': '../../frameworks/bootstrap/js/bootstrap.min',
        'angularBootstrap': '../../frameworks/angular-bootstrap/ui-bootstrap-tpls',
        'angularAnimate': '../../frameworks/angular-animate/angular-animate',
        'angularResource': '../../frameworks/angular-resource/angular-resource'
    },

    shim: {
        'angular': {
            exports: 'angular'
        },
        'jBootstrap': {
            deps: ['jQuery']
        },
        'angular-route': {
            deps: ['angular']
        },
        'angularAnimate': {
            deps: ['angular']
        },
        'angularResource': {
            deps: ['angular']
        },
        'angularBootstrap': {
            deps: ['angular']
        }
    },

    deps: ['./bootstrap']
});